package lesson1

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func Search(str string, sites []string) []string {
	var matched []string
	for i := range sites {

		resp, err := http.Get(sites[i])
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
			return nil
		}

		htmlData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil
		}

		err = resp.Body.Close()
		if err != nil {
			fmt.Println(err)
		}

		//matched, err := regexp.MatchString(str, string(htmlData))
		contains := strings.Contains(string(htmlData), str)

		if contains {
			matched = append(matched, sites[i])
		}
	}
	return matched
}
