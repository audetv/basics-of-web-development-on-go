package main

import (
	"fmt"
	"gitlab.com/audetv/basics-of-web-development-on-go/lesson1"
)

func main() {
	sites := []string{"https://yandex.ru", "https://mail.ru"}

	matched := lesson1.Search("Афиша", sites)

	fmt.Println(matched)
}
